package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TimeSheetPK implements Serializable{

	private int idUser;
	private int idProjet;
	private String nomTache;
	public TimeSheetPK() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TimeSheetPK(int idUser, int idProjet, String nomTache) {
		super();
		this.idUser = idUser;
		this.idProjet = idProjet;
		this.nomTache = nomTache;
	}

	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdProjet() {
		return idProjet;
	}
	public void setIdProjet(int idProjet) {
		this.idProjet = idProjet;
	}
	public String getNomTache() {
		return nomTache;
	}
	public void setNomTache(String nomTache) {
		this.nomTache = nomTache;
	}
	
	
	
	
}
