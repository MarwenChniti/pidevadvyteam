package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class QuestionReponseUser implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int idQR;
	
	private String question ; 
	
	private String reponse ;
	
	private boolean status;
	

	public int getIdQR() {
		return idQR;
	}

	public void setIdQR(int idQR) {
		this.idQR = idQR;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public QuestionReponseUser(int idQR, String question, String reponse) {
		super();
		this.idQR = idQR;
		this.question = question;
		this.reponse = reponse;
	}

	public QuestionReponseUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	} 
	
	
	
	
	
}
