package tn.esprit.advyteam.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Quiz implements Serializable{

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id_Quiz")
	
	private int idQuiz ; 
	private String nom ;
	
	private String competance ;

	@OneToMany(fetch=FetchType.EAGER , cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="Id_quiz")
	private List<QuestionReponseQuiz> questionReponseQuizList= new ArrayList<>() ; 
	
	
	
	
	public int getIdQuiz() {
		return idQuiz;
	}
	public void setIdQuiz(int idQuiz) {
		this.idQuiz = idQuiz;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Quiz() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Quiz(String nom) {
		super();
	this.nom=nom;
	
	}
	public Quiz(int idQuiz, String nom) {
		super();
		this.idQuiz = idQuiz;
		this.nom = nom;
	}
	public List<QuestionReponseQuiz> getQuestionReponseQuizList() {
		return questionReponseQuizList;
	}
	public void setQuestionReponseQuizList(List<QuestionReponseQuiz> questionReponseQuizList) {
		this.questionReponseQuizList = questionReponseQuizList;
	}
	public String getCompetance() {
		return competance;
	}
	public void setCompetance(String competance) {
		this.competance = competance;
	} 
	
	
	
}
