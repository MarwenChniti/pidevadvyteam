package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity

public class Participation implements Serializable {
	@EmbeddedId
	private FormationPK formationpk ;
	
	@ManyToOne
	@JoinColumn(name="idUser",referencedColumnName="id",insertable=false,updatable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="idFormation",referencedColumnName="id",insertable=false,updatable=false)
	private Formation formation;

	public Participation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FormationPK getFormationpk() {
		return formationpk;
	}

	public void setFormationpk(FormationPK formationpk) {
		this.formationpk = formationpk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	
	

}
