package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Commentaire implements Serializable {

	
	private int id;
	private String coment;
	private User user;
	private Publication publication;

	public Commentaire(int id, String coment, User user, Publication publication) {
		this.id = id;
		this.coment = coment;
		this.user = user;
		this.publication = publication;
	}

	public Commentaire(String coment) {
		this.coment = coment;
	}
	public Commentaire(String coment,User user,Publication publication) {
		this.coment = coment;
		this.user= user;
		this.publication= publication;
	}

	@ManyToOne
	public Publication getPublication() {
		return publication;
	}

	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComent() {
		return coment;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	@ManyToOne
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Commentaire() {
		super();

	}

}
