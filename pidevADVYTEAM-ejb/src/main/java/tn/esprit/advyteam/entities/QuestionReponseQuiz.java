package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class QuestionReponseQuiz implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int idQR;
	
	private String question ; 
	
	private String reponse ;
	
	private String option1;
	private String option2;
	private String option3;

	public int getIdQR() {
		return idQR;
	}

	public void setIdQR(int idQR) {
		this.idQR = idQR;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public QuestionReponseQuiz(int idQR, String question, String reponse) {
		super();
		this.idQR = idQR;
		this.question = question;
		this.reponse = reponse;
	}

	public QuestionReponseQuiz() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	} 
	
	
	
	
	
	

}
