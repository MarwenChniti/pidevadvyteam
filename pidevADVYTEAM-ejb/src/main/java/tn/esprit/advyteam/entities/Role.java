package tn.esprit.advyteam.entities;

public enum Role {
	RessourcesHumaine,
	Employe,
	Manager
}
