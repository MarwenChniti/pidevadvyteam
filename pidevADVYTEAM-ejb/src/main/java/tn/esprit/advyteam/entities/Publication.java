package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Publication implements Serializable {

	private int id;
	private String statut;
	private String image;
	public Publication(int id, String statut, String image, User user, List<Commentaire> commentaires,
			CentreInter centreinter) {
		super();
		this.id = id;
		this.statut = statut;
		this.image = image;
		this.user = user;
		this.commentaires = commentaires;
		this.centreinter = centreinter;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private User user;
	private List<Commentaire> commentaires;
	private CentreInter centreinter;

	@ManyToOne
	public CentreInter getCentreinter() {
		return centreinter;
	}

	public void setCentreinter(CentreInter centreinter) {
		this.centreinter = centreinter;
	}

	public Publication(int id, String statut, User user) {
		this.id = id;
		this.statut = statut;
		this.user = user;
	}

	public Publication(String statut) {

		this.statut = statut;

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	

	public Publication(String statut, User user) {
		this.statut = statut;
		this.user = user;
	}

	public Publication(String statut, User user, List<Commentaire> commentaires) {
		this.statut = statut;
		this.user = user;
		this.commentaires = commentaires;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}
	
	@ManyToOne
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

	public Publication(int id, String statut, User user, List<Commentaire> commentaires, CentreInter centreinter) {
		super();
		this.id = id;
		this.statut = statut;
		this.user = user;
		this.commentaires = commentaires;
		this.centreinter = centreinter;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "publication", cascade = CascadeType.ALL)
	public List<Commentaire> getCommentaires() {
		return commentaires;
	}
	


	public Publication() {
		super();
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

}
