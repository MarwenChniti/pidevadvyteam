package tn.esprit.advyteam.entities;





import javax.persistence.*;




@Entity
public class Evaluation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Enumerated(EnumType.STRING)
	private EvalType evaltype;
	private String startDate;
	private String endDate;
	
	private  Boolean state ;

	
	//---Mapping to User ---
	 @ManyToOne
		private User user;
	//----ToString-----
	

		public Evaluation() {
			super();
		}

	    public Evaluation(EvalType evaltype, String startDate, String endDate, Boolean state) {
			super();
			this.evaltype = evaltype;
			this.startDate = startDate;
			this.endDate = endDate;
			this.state = state;
		
		}
	
	// ----Constructors-----

	
	@Override
	public String toString() {
		return "Evaluation [id=" + id + ", evaltype=" + evaltype + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", user=" + user + ", state=" + state +"]";
	}




	// ----Getters & Setter------
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EvalType getEvaltype() {
		return evaltype;
	}

	public void setEvaltype(EvalType evaltype) {
		this.evaltype = evaltype;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	


}