package tn.esprit.advyteam.entities;

public enum Etat {
	To_Do,
	In_Progress,
	Done
}
