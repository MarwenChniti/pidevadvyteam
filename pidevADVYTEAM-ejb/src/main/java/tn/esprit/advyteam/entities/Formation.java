package tn.esprit.advyteam.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Formation {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	private String nom ;
	private String descreption ;
	private String date ;
	private String lieu ;
	private int nbrplace ;
	private boolean etat ;
	@Enumerated
	private Type type ;
	@JsonIgnore
	@ManyToOne
	private Categorie categorie ;
	//@ManyToMany(mappedBy="formations") 
	//private List<User> users ;
	@JsonIgnore
	@OneToMany(mappedBy="formation",fetch=FetchType.EAGER)
	private List<Participation> participations ;
	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}



	

	@Override
	public String toString() {
		return "Formation [id=" + id + ", nom=" + nom + ", descreption=" + descreption + "]";
	}





	public Formation(String nom, String descreption, String lieu, int nbrplace, boolean etat,String date , Type type) {
		super();
		this.nom = nom;
		this.descreption = descreption;
		this.lieu = lieu;
		this.nbrplace = nbrplace;
		this.etat = etat;
		this.date=date ;
		this.type = type ;
	}





	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescreption() {
		return descreption;
	}

	public void setDescreption(String descreption) {
		this.descreption = descreption;
	}

	

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public int getNbrplace() {
		return nbrplace;
	}
	public void setNbrplace(int nbrplace) {
		this.nbrplace = nbrplace;
	}
	





	public boolean isEtat() {
		return etat;
	}





	public void setEtat(boolean etat) {
		this.etat = etat;
	}





	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}





	public Type getType() {
		return type;
	}





	public void setType(Type type) {
		this.type = type;
	}





	public List<Participation> getParticipations() {
		return participations;
	}





	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}





	
	
	

}
