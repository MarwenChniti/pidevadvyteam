package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class QuizUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id_QuizUser")
	private int idQuizUser ; 
	
	private String nom;
	
	private int userID ; 
	
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="Id_quiz_user")
	private List<QuestionReponseUser> questionReponseUserList=new ArrayList<QuestionReponseUser>() ;

	
	
	public int getIdQuizUser() {
		return idQuizUser;
	}

	public void setIdQuizUser(int idQuizUser) {
		this.idQuizUser = idQuizUser;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<QuestionReponseUser> getQuestionReponseUserList() {
		return questionReponseUserList;
	}

	public void setQuestionReponseUserList(List<QuestionReponseUser> questionReponseUserList) {
		this.questionReponseUserList = questionReponseUserList;
	}

	public QuizUser(int idQuizUser, String nom, List<QuestionReponseUser> questionReponseUserList) {
		super();
		this.idQuizUser = idQuizUser;
		this.nom = nom;
		this.questionReponseUserList = questionReponseUserList;
	}

	public QuizUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuizUser(String nom) {
		super();
		
		this.nom = nom;
		
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}


	
	

}
