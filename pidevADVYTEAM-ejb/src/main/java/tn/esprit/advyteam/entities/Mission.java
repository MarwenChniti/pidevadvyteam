package tn.esprit.advyteam.entities;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import java.io.Serializable;
import java.util.Date;




@Entity

public class Mission implements  Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	private String objet ;
	private String description ;
	private String date ;
	private String lieu ;
	@ManyToMany(mappedBy="missions") 
	private List<User> users ;
	@OneToMany(mappedBy = "mission")
	private List<Rapport>rapports;
	
	



	public List<Rapport> getRapports() {
		return rapports;
	}



	public void setRapports(List<Rapport> rapports) {
		this.rapports = rapports;
	}



	public Mission() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Mission(String objet, String description, String date, String lieu) {
		super();
		this.objet = objet;
		this.description = description;
		this.date = date;
		this.lieu = lieu;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "Mission [id=" + id + ", objet=" + objet + ", description=" + description + ", date=" + date + ", lieu="
				+ lieu + "]";
	}
	
	

}
