package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class CentreInter implements Serializable {
	private int id ;
	private String nom ;
	
	private List<Publication> publications;
	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "centreinter", cascade = CascadeType.ALL)
	public List<Publication> getPublications() {
		return publications;
	}


	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setId(int id) {
		this.id = id;
	}


	public CentreInter() {
		super();
		
	}


	public CentreInter(int id, String nom, List<Publication> publications) {
		super();
		this.id = id;
		this.nom = nom;
		this.publications = publications;
	}

	
	
	
}
