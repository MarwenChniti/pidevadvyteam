package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.management.Query;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class TimeSheet implements Serializable {
	
	@EmbeddedId
	private TimeSheetPK timeSheetPK;
	
	@Enumerated
	private Etat etat;
	private String descTache;
	private int nbrHeures;
	private boolean isValid;
	
	@ManyToOne
	@JoinColumn(name="idUser",referencedColumnName="id",insertable=false,updatable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="idProjet",referencedColumnName="idProjet",insertable=false,updatable=false)
	private Projet projet;
	
	public TimeSheet() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public TimeSheet(Etat etat, String descTache, int nbrHeures, User user, Projet projet) {
		super();
		this.etat = etat;
		this.descTache = descTache;
		this.nbrHeures = nbrHeures;
		this.user = user;
		this.projet = projet;
	}


	public TimeSheetPK getTimeSheetPK() {
		return timeSheetPK;
	}


	public void setTimeSheetPK(TimeSheetPK timeSheetPK) {
		this.timeSheetPK = timeSheetPK;
	}


	public Etat getEtat() {
		return etat;
	}


	public void setEtat(Etat etat) {
		this.etat = etat;
	}


	public String getDescTache() {
		return descTache;
	}


	public void setDescTache(String descTache) {
		this.descTache = descTache;
	}


	public int getNbrHeures() {
		return nbrHeures;
	}


	public void setNbrHeures(int nbrHeures) {
		this.nbrHeures = nbrHeures;
	}


	public TimeSheet(TimeSheetPK timeSheetPK, Etat etat, String descTache, int nbrHeures,
			boolean isValid, User user, Projet projet) {
		super();
		this.timeSheetPK = timeSheetPK;
		
		this.etat = etat;
		this.descTache = descTache;
		this.nbrHeures = nbrHeures;
		this.isValid = isValid;
		this.user = user;
		this.projet = projet;
	}



	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}



	
	
}

