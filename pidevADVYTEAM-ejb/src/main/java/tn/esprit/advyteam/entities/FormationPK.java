package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable

public class FormationPK implements Serializable {
	private int idUser;
	private int idFormation;
	private String nomFormation;
	
	public FormationPK() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FormationPK(int idUser, int idFormation, String nomFormation) {
		super();
		this.idUser = idUser;
		this.idFormation = idFormation;
		this.nomFormation = nomFormation;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdFormation() {
		return idFormation;
	}
	public void setIdFormation(int idFormation) {
		this.idFormation = idFormation;
	}
	public String getNomFormation() {
		return nomFormation;
	}
	public void setNomFormation(String nomFormation) {
		this.nomFormation = nomFormation;
	}
	
	
	
	
	
	

}
