package tn.esprit.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Competences implements Serializable {
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id_competences")
	private int idCompetence ; 
	
	private String comptencePersonel; 
	
	private String description; 
	
	@Enumerated(EnumType.STRING) 
	private Niveau niveau ;
	
	
	

	public int getIdCompetence() {
		return idCompetence;
	}

	public void setIdCompetence(int idCompetence) {
		this.idCompetence = idCompetence;
	}

	public String getComptencePersonel() {
		return comptencePersonel;
	}

	public void setComptencePersonel(String comptencePersonel) {
		this.comptencePersonel = comptencePersonel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	public Competences() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Competences(int idCompetence, String comptencePersonel, String description, Niveau niveau) {
		super();
		this.idCompetence = idCompetence;
		this.comptencePersonel = comptencePersonel;
		this.description = description;
		this.niveau = niveau;
	} 
	
	public Competences(String comptencePersonel, String description, Niveau niveau) {
		super();
		
		this.comptencePersonel = comptencePersonel;
		this.description = description;
		this.niveau = niveau;
	} 
	

}
