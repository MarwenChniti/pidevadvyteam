package tn.esprit.advyteam.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class User implements Serializable{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	private String image;
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImageCouverture() {
		return imageCouverture;
	}
	public void setImageCouverture(String imageCouverture) {
		this.imageCouverture = imageCouverture;
	}
	private String imageCouverture="default.png";
	private String prenom, nom,login,password,addresse,email,cin;
	private Date dat_naiss;
	@Enumerated
	private Role role;
	//@ManyToMany
	//private List<Formation> formations ;
	@ManyToMany
	private List<Rapport> rapports ;
	@ManyToMany
	private List<Mission> missions ;
	@OneToMany(mappedBy="user")
	private List<Evaluation> evaluations ;
	
	@OneToMany(mappedBy="user")
	private List<Participation> participations ;

	@OneToMany(mappedBy="user") 
	private List<Commentaire> commentaires ;
	
	@OneToMany(mappedBy="user")
	private List<TimeSheet> timesheets;
	
	public User(int id, String prenom, String nom, String login, String password, String addresse, String email,
			String cin, Date dat_naiss, List<Commentaire> commentaires, List<Publication> publications, User manager,
			List<User> employees, Role role) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.addresse = addresse;
		this.email = email;
		this.cin = cin;
		this.dat_naiss = dat_naiss;
		this.commentaires = commentaires;
		this.publications = publications;
		this.manager = manager;
		this.employees = employees;
		this.role = role;
	}
	public User(String prenom, String nom, String login, String password, String addresse, String email, String cin,
			Date dat_naiss, List<Commentaire> commentaires, List<Publication> publications, User manager,
			List<User> employees, Role role) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.addresse = addresse;
		this.email = email;
		this.cin = cin;
		this.dat_naiss = dat_naiss;
		this.commentaires = commentaires;
		this.publications = publications;
		this.manager = manager;
		this.employees = employees;
		this.role = role;
	}
	public List<Commentaire> getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}
	@OneToMany(mappedBy="user")
	private List <Publication> publications ;
	public List<Publication> getPublications() {
		return publications;
	}
	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}
	public User getManager() {
		return manager;
	}
	public void setManager(User manager) {
		this.manager = manager;
	}
	public List<User> getEmployees() {
		return employees;
	}
	public void setEmployees(List<User> employees) {
		this.employees = employees;
	}
	@ManyToOne
	@JoinColumn(name ="manager_id")
	private User manager;

	@OneToMany(mappedBy = "manager" , fetch = FetchType.LAZY)  
	private List<User> employees;
	
	public User(String prenom, String nom, String login, String password, String addresse, String email, String cin,
			Date dat_naiss, Role role) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.addresse = addresse;
		this.email = email;
		this.cin = cin;
		this.dat_naiss = dat_naiss;
		this.role = role;
	}
	public User(String cin,String prenom, String nom, String login, String password, String addresse, String email,Role role,
			Integer idd) {
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.addresse = addresse;
		this.email = email;
		this.cin = cin;
		this.id= idd ;
		this.role = role;
	}
	public String getAddresse() {
		return addresse;
	}
	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public Date getDat_naiss() {
		return dat_naiss;
	}
	public void setDat_naiss(Date dat_naiss) {
		this.dat_naiss = dat_naiss;
	}


	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int id, String prenom, String nom, String login, String password, Role role) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.role = role;
	}
	public User(String prenom, String nom, String login, String password, Role role) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.login = login;
		this.password = password;
		this.role = role;
	}
	
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public List<Participation> getParticipations() {
		return participations;
	}
	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}

	public List<Rapport> getRapports() {
		return rapports;
	}
	public void setRapports(List<Rapport> rapports) {
		this.rapports = rapports;
	}
	public List<Mission> getMissions() {
		return missions;
	}
	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}
	public List<Evaluation> getEvaluations() {
		return evaluations;
	}
	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}
	
	

}

