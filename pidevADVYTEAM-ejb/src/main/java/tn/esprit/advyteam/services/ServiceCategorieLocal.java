package tn.esprit.advyteam.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.advyteam.entities.Categorie;
import tn.esprit.advyteam.entities.Formation;


@Local
public interface ServiceCategorieLocal {
	 public void addCategorie(Categorie c);
	 public void removeCategorie(int id);
		
	 public void updateCategorie(Categorie c);
	 public List<Categorie> getAllcategorie();
}
