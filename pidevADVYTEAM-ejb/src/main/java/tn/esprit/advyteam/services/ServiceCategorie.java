package tn.esprit.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.advyteam.entities.Categorie;
import tn.esprit.advyteam.entities.Formation;

/**
 * Session Bean implementation class ServiceCategorie
 */
@Stateless
@LocalBean
public class ServiceCategorie implements ServiceCategorieRemote, ServiceCategorieLocal {
	@PersistenceContext
	EntityManager em ;	
    /**
     * Default constructor. 
     */
    public ServiceCategorie() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addCategorie(Categorie c) {
		em.persist(c);
		
	}

	@Override
	public void removeCategorie(int id) {
		em.remove(em.find(Categorie.class , id));
		
	}

	@Override
	public void updateCategorie(Categorie c) {
		em.merge(c);
		
	}

	@Override
	public List<Categorie> getAllcategorie() {
		TypedQuery<Categorie> query =
				em.createQuery("select c from Categorie c",Categorie.class);
		return query.getResultList();
	}
	

}
