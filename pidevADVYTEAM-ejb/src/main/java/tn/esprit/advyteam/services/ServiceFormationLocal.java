package tn.esprit.advyteam.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.advyteam.entities.Formation;

@Local
public interface ServiceFormationLocal {
	 public void addformation(Formation f);
	 public void removeformation(int id);
	
	 public void updateformation(Formation f);
	 public Number getAllformationsDone();
	 public Number getAllformationsUndone() ;
	 public List<Formation> getAllformation();
	 public Integer getAllformationsbyCategorie();
	 public int addF(Formation evala);
	 
	

}
