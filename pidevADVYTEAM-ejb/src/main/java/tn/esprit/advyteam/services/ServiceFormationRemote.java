package tn.esprit.advyteam.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.advyteam.entities.Formation;

@Remote
public interface ServiceFormationRemote {
	 public void addformation(Formation f);
	 public void removeformation(int id);
	
	 public void updateformation(Formation f);
	 public Number getAllformationsDone();
	 public Number getAllformationsUndone() ;
	 public List<Formation> getAllformation();
	 

}
