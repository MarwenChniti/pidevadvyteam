package tn.esprit.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.advyteam.entities.Formation;
import tn.esprit.advyteam.entities.FormationPK;
import tn.esprit.advyteam.entities.Participation;
import tn.esprit.advyteam.entities.Role;
import tn.esprit.advyteam.entities.User;

/**
 * Session Bean implementation class ServiceParticipation
 */
@Stateless
@LocalBean
public class ServiceParticipation implements ServiceParticipationRemote, ServiceParticipationLocal {
	@PersistenceContext
	EntityManager em ;	
	List<Participation> T;
    /**
     * Default constructor. 
     */
    public ServiceParticipation() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addParticipation(int formationId, int userId, String nomFormation) {
		
		FormationPK formationPK= new FormationPK();
		formationPK.setIdFormation(formationId);
		formationPK.setIdUser(userId);
		formationPK.setNomFormation(nomFormation);
		Participation participation = new Participation();
		participation.setFormationpk(formationPK);
		
		em.persist(participation);
		
	}

	@Override
	public Participation getParticipationByFormation(int id) {
		Participation aa =new Participation();
		Query query =  em.createQuery("select t from Participation t",Participation.class);
		
		
		
		T= query.getResultList();
		for (Participation t:T) {
			if(t.getFormation().getId()==id)
			{
				aa=t;
			}
		
		}
		return aa;
		
	}
	

}
