package tn.esprit.advyteam.services;

import javax.ejb.Local;

import tn.esprit.advyteam.entities.Participation;

@Local
public interface ServiceParticipationLocal {
	public void addParticipation(int formationId, int userId,  String nomFormation );
    public Participation getParticipationByFormation(int id);
}
