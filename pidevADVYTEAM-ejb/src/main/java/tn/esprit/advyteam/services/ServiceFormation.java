package tn.esprit.advyteam.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.advyteam.entities.Formation;

/**
 * Session Bean implementation class ServiceFormation
 */
@Stateless
@LocalBean
public class ServiceFormation implements ServiceFormationRemote, ServiceFormationLocal {

	
	List<Formation> formations = new ArrayList<Formation>();
    /**
     * Default constructor. 
     */
	@PersistenceContext
	EntityManager em ;		
    public ServiceFormation() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public void addformation(Formation f) {
		
		em.persist(f);
		
	}
	@Override
	public void removeformation(int id) {
		em.remove(em.find(Formation.class , id));
		
	}
	
	
	
	
	@Override
	public void updateformation(Formation f) {
		em.merge(f);
		
		
	}
	@Override
	public Integer getAllformationsDone() {
		Query query = em.createQuery("select e from Formation e where e.etat=:etat",Formation.class);
		query.setParameter("etat",true);
		
	 
		return query.getResultList().size();//(Number)query.getSingleResult();
	}
	@Override
	public Integer getAllformationsUndone() {
		Query query = em.createQuery("select e from Formation e where e.etat=:etat",Formation.class);
		query.setParameter("etat",false);
		return query.getResultList().size();
	}
	
	@Override
	public List<Formation> getAllformation() {
		TypedQuery<Formation> query =
				em.createQuery("select e from Formation e",
						Formation.class);
		List<Formation> resultat= query.getResultList();
		return resultat;
	}
	
	@Override
	public Integer getAllformationsbyCategorie() {
		Query query = em.createQuery("select e from Formation e where e.type=:type",Formation.class);
		query.setParameter("type",0);
		return query.getResultList().size();
	}
	
	public List<Formation> rechecherFormation(String fom) {
		TypedQuery<Formation> query = em.createQuery("Select t from Formation t where t.nom=:fom", Formation.class);
		query.setParameter("fom", fom);
		return query.getResultList();

	}
	
	
	public int addF(Formation evala) {
		// TODO Auto-generated method stub
		System.out.println("eval ajouté");
		em.persist(evala);
		return evala.getId();
	


	}
}
