package Ressources;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.esprit.advyteam.entities.Formation;
import tn.esprit.advyteam.services.ServiceFormationLocal;

@Path("formation")
@RequestScoped
public class FormationRessource {
	@EJB
	ServiceFormationLocal cm;

	public FormationRessource() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response AllFormation() {
		return Response.ok().entity(cm.getAllformation()).build();
	}
	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteFormation(@PathParam("id") int id) {
	
		    cm.removeformation(id);
			return Response.status(Response.Status.OK).entity("la suppression est effectuée").build();
		
	}
	
	
	@POST
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEvaluation (Formation e) throws Exception{
		return Response.status(Status.OK).entity(cm.addF(e)).build();
	}
	
	}
	
	
	
	
	
	
	
	
	

