package ManagedBean;


import java.util.List;


import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

//import org.primefaces.model.chart.AxisType;
//import org.primefaces.model.chart.BarChartModel;
//import org.primefaces.model.chart.ChartSeries;

import tn.esprit.advyteam.entities.Formation;
import tn.esprit.advyteam.entities.Type;
import tn.esprit.advyteam.services.ServiceFormation;


@ManagedBean
@SessionScoped
public class FormationBean {
    private int id ;
	private String nom ;
	private String Descreption ;
	private String lieu ;
	private String date ;
	private int nbrplace ;
	private boolean etat ;
	private Type type ;
	private List<Formation> formationss ;
	private boolean affiche ;
	private int formToUpdate;
	private Formation evala= new Formation();
	private Formation selectedCar;
	
	
	 
		
@EJB 
 ServiceFormation serviceformation ;


public void addFormation() {
	
	serviceformation.addformation(new Formation( nom, Descreption, lieu, nbrplace, etat,date, type));
	FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Formation ajoutée avec succès !"));
	

}
public String returnList() {
	String navigateTo = null ;
	navigateTo = "/formation/affichage?faces-redirect=true";
	return navigateTo ;

}



public List<Formation> getformations() {
	 formationss = serviceformation.getAllformation();
	 System.out.println(formationss);
return formationss ; 

}



public void remove(Integer	id) {
	serviceformation.removeformation(id);
}

public void modifier(Formation f) {
	affiche = true;
	this.setNom(f.getNom());
	this.setDescreption(f.getDescreption());
	this.setEtat(f.isEtat());
	this.setDate(f.getDate());
	this.setLieu(f.getLieu());
	this.setNbrplace(f.getNbrplace());
	this.setId(f.getId());
	this.setType(f.getType());
	System.out.println("ID est"+ id);
	}

public void mettreAjour() {
	System.out.println("méthode mettreajourID est"+ id);
	//evalService.updateEvaluation(new EvaluationAnnuel(id, nom,  description,  etat,  type, employee, objectif,  rendezVous,  dateEcheance));
evala.setId(id);
evala.setNom(nom);
evala.setDescreption(Descreption);
evala.setEtat(false);
evala.setDate(date);
evala.setLieu(lieu);
evala.setNbrplace(nbrplace);
evala.setType(type);
serviceformation.updateformation(evala);


}








public FormationBean(int id, String nom, String descreption, String lieu, String date, int nbrplace, boolean etat,
		Type type) {
	super();
	this.id = id;
	this.nom = nom;
	Descreption = descreption;
	this.lieu = lieu;
	this.date = date;
	this.nbrplace = nbrplace;
	this.etat = etat;
	this.type = type;
}

public FormationBean() {
	super();
	// TODO Auto-generated constructor stub
}

public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getDescreption() {
	return Descreption;
}
public void setDescreption(String descreption) {
	Descreption = descreption;
}
public String getLieu() {
	return lieu;
}
public void setLieu(String lieu) {
	this.lieu = lieu;
}
public int getNbrplace() {
	return nbrplace;
}
public void setNbrplace(int nbrplace) {
	this.nbrplace = nbrplace;
}



public boolean isEtat() {
	return etat;
}

public void setEtat(boolean etat) {
	this.etat = etat;
}

public ServiceFormation getServiceformation() {
	return serviceformation;
}
public void setServiceformation(ServiceFormation serviceformation) {
	this.serviceformation = serviceformation;
}

public String getDate() {
	return date;
}

public void setDate(String date) {
	this.date = date;
}


public List<Formation> getFormationss() {
	return formationss;
}

public void setFormationss(List<Formation> formationss) {
	this.formationss = formationss;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}



public boolean isAffiche() {
	return affiche;
}

public void setAffiche(boolean affiche) {
	this.affiche = affiche;
}

public int getFormToUpdate() {
	return formToUpdate;
}

public void setFormToUpdate(int formToUpdate) {
	this.formToUpdate = formToUpdate;
}

public Formation getEvala() {
	return evala;
}

public void setEvala(Formation evala) {
	this.evala = evala;
}

public Type getType() {
	return type;
}

public void setType(Type type) {
	this.type = type;
}

public Formation getSelectedCar() {
	return selectedCar;
}

public void setSelectedCar(Formation selectedCar) {
	this.selectedCar = selectedCar;
}








	/*
	 * private void createBar() { bar = new BarChartModel(); ChartSeries neworder =
	 * new ChartSeries("new1") ; neworder.set("Done", dogetDone());
	 * neworder.set("Done", dogetUnDone());
	 * 
	 * 
	 * bar.addSeries(neworder); bar.setTitle("Poejcts by State ");
	 * bar.setLegendPosition("ne");
	 * 
	 * bar.getAxis(AxisType.X).setLabel("YEAR");
	 * bar.getAxis(AxisType.Y).setLabel("Project State");
	 * 
	 * bar.getAxis(AxisType.X).setMin(0); bar.getAxis(AxisType.Y).setMax(10);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

}



