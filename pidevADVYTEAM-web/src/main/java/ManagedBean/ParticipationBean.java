package ManagedBean;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import tn.esprit.advyteam.entities.Participation;
import tn.esprit.advyteam.services.ServiceParticipation;

@ManagedBean
public class ParticipationBean implements Serializable {
	private Participation t = new Participation();
	private int formationId,userId;
	private String nomFormation;
	@EJB
	ServiceParticipation serviceparticipation ;
	
	public void addParticipation() {
		serviceparticipation.addParticipation(formationId, userId, nomFormation);
	}
	
	public void Redirect() throws IOException
	{
		FacesContext.getCurrentInstance().getExternalContext().redirect("Participation.xhtml");
	}
	public void Aff(int formationId)  {
		t=serviceparticipation.getParticipationByFormation(formationId);
		System.out.println(t);
		Redirect.T=t;
		try {
			Redirect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int getFormationId() {
		return formationId;
	}

	public void setFormationId(int formationId) {
		this.formationId = formationId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNomFormation() {
		return nomFormation;
	}

	public void setNomFormation(String nomFormation) {
		this.nomFormation = nomFormation;
	}

	public ServiceParticipation getServiceparticipation() {
		return serviceparticipation;
	}

	public void setServiceparticipation(ServiceParticipation serviceparticipation) {
		this.serviceparticipation = serviceparticipation;
	}

	public Participation getT() {
		return t;
	}

	public void setT(Participation t) {
		this.t = t;
	}
	
	
}
