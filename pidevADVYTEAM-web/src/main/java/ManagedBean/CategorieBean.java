package ManagedBean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.advyteam.entities.Categorie;
import tn.esprit.advyteam.services.ServiceCategorie;
@ManagedBean
@SessionScoped
public class CategorieBean {
	private int id ;
	private String nom , descreption ;
	private List<Categorie> categories ;
	@EJB 
	ServiceCategorie servicecategorie ;
	
	public String addcategorie() {
		String navigateTo = null ;
		servicecategorie.addCategorie(new Categorie(nom , descreption));
		navigateTo = "/categorie/listcategorie?faces-redirect=true";
		return navigateTo ;
	}
	
	public void remove(Integer	id) {
		servicecategorie.removeCategorie(id);
		
	}
	
	 

	public List<Categorie> getcate(){
		categories = servicecategorie.getAllcategorie();
		 System.out.println(categories);
		 return categories ;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescreption() {
		return descreption;
	}

	public void setDescreption(String descreption) {
		this.descreption = descreption;
	}

	

	public List<Categorie> getCategories() {
		return categories;
	}

	public void setCategories(List<Categorie> categories) {
		this.categories = categories;
	}

	public ServiceCategorie getServicecategorie() {
		return servicecategorie;
	}

	public void setServicecategorie(ServiceCategorie servicecategorie) {
		this.servicecategorie = servicecategorie;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
