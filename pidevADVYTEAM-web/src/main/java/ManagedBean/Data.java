package ManagedBean;


import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.advyteam.entities.Type;

@ManagedBean
@ApplicationScoped
public class Data {

	public Type[] getTypes() {
		return Type.values();
	}
}
