package ManagedBean;


import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.advyteam.entities.Participation;



/**
 * Session Bean implementation class Redirect
 */
@ManagedBean
@ApplicationScoped
public class Redirect {
public static Participation T ;
    /**
     * Default constructor. 
     */
    public Redirect() {
        // TODO Auto-generated constructor stub
    }
	public Participation getT() {
		return T;
	}
	public void setT(Participation t) {
		T = t;
	}

}
